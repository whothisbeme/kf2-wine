#!/bin/bash

START='/home/xclient/start.sh'
SCREEN_NAME="kf2server"

while /bin/true; do

    sleep 10
    echo "checking..."
    SERVER=`ps --User root | grep "KFServer.exe" | grep -v grep | wc -l`

    if [[ $SERVER -eq "0" ]]; then
        screen -S $SCREEN_NAME -X quit
        screen -dmLS $SCREEN_NAME $START
        DATE_NOW=`date`
        echo "Restarted: $DATE_NOW" >> restarted_kf2.log
        sleep 10
    fi
done
