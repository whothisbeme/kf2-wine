FROM suchja/wine

MAINTAINER Ryan Merrit <ryan.merritt@whothisbeme.xyz>

USER root

# Make server port available to host
EXPOSE 27015 7777

# Install dependencies
RUN apt-get update &&\
    apt-get install -y wget unzip

# Download and extract SteamCMD
RUN mkdir -p /home/xclient/steamcmd &&\
    cd /home/xclient/steamcmd &&\
    wget --no-check-certificate https://steamcdn-a.akamaihd.net/client/installer/steamcmd.zip &&\
    unzip steamcmd.zip &&\
    rm steamcmd.zip

#Add Files
ADD installKF2.sh .
ADD autoRestart.sh .
ADD start.sh .
ADD kf2_ds.txt .

# Wine really doesn't like to be run as root, so let's use a non-root user
USER xclient

#install kf2server
#RUN ./installKF2.sh

#Copy kf2-dlls to wine
RUN wget http://www.redorchestra2.fr/downloads/KF2_WineDLL.zip
    #unzip KF2_WineDLL.zip -d ~/.wine/drive_c/windows/system32

RUN echo "alias winegui='wine explorer /desktop=DockerDesktop,1024x768'" > ~/.bash_aliases 

#Install VCRUN2010
#RUN winetricks -q vcrun2010
