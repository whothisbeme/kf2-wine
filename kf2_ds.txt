// kf2_ds.txt
//
@ShutdownOnFailedCommand 1 //set to 0 if updating multiple servers at once
@NoPromptForPassword 1
//for servers which don't need a login
login anonymous
force_install_dir ./kf2server
app_update 232130 validate
quit
