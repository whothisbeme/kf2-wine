# Killing Floor 2

The Dockerfile will build an image for running a Killing Floor 2 dedicated server with the most recent Wine 1.7 beta.

It behaves like an executable, so just pass it args. An example to start a classic casual server:

```
docker run -it mcpackin/kf2-wine (Args)
```
wine /home/MyUser/steamcmdwin/kf2server/Binaries/Win64/KFServer kf-bioticslab?difficulty=0 -port=7777
